package ejemplo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Email {
	
	public void ValidarEmail(String email) {
		
		String expRegularEmail = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@" +
			      "[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
		Pattern patron = Pattern.compile(expRegularEmail);
		
		if (email != null) {
		   Matcher matcher = patron.matcher(email);
		   if (matcher.matches()) 
		     System.out.println("Email V�lido");
		   else 
		     System.out.println("Email NO V�lido");
		}
	}
}
