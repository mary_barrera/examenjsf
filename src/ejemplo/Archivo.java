package ejemplo;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name="Archivo")
public class Archivo {

	public Archivo() {}
	
	public String someText;

	public String getSomeText() {
		return someText;
	}
	public void setSomeText(String someText) {
		this.someText = someText;
	}

	public static void crearArchivo(String cadenaTexto) {
		String nombreArchivo = "Salida.txt";
		try {
			PrintWriter printer = new PrintWriter(nombreArchivo);
			printer.println("Hola Usuario :)");
			printer.println(cadenaTexto);
			printer.close();
			printer.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
